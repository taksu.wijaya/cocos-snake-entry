// shopeeWebBridge@1.0.0
(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
    typeof define === 'function' && define.amd ? define('shopee-web-bridge', ['exports'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global.ShopeeWebBridge = {}));
})(this, (function (exports) { 'use strict';

    //
    // Polyfills for legacy environments
    //
    /*
     * Support Android 4.4.x
     */
    if (!ArrayBuffer.isView) {
        ArrayBuffer.isView = function (a) {
            return a !== null && typeof (a) === 'object' && a.buffer instanceof ArrayBuffer;
        };
    }
    // Define globalThis if not available.
    // https://github.com/colyseus/colyseus.js/issues/86
    if (typeof (globalThis) === "undefined" &&
        typeof (window) !== "undefined") {
        // @ts-ignore
        window['globalThis'] = window;
    }

    var CUSTOM_PROTOCOL_SCHEME = "wvjbscheme",
      QUEUE_HAS_MESSAGE = "__WVJB_QUEUE_MESSAGE__";
    !(function () {
      console.log("register web bridge");
      var e;
      window.inBeeShop = function () {
        return (
          window &&
          window.navigator &&
          window.navigator.userAgent &&
          -1 !== window.navigator.userAgent.toLowerCase().indexOf("beeshop")
        );
      };
      var n = [],
        i = [],
        a = {},
        r = {},
        t = 1;
      var d = {};
      function o(i, a) {
        if (!window.__appHasPendingCacheUpdate__) {
          if (a) {
            var d = "cb_" + t++ + "_" + +new Date();
            (r[d] = a), (i.callbackId = d);
          }
          -1 === window.navigator.userAgent.toLowerCase().indexOf("android")
            ? (n.push(i),
              (e.src = CUSTOM_PROTOCOL_SCHEME + "://" + QUEUE_HAS_MESSAGE))
            : window.gabridge && window.gabridge.sendMsg(JSON.stringify(i));
        }
      }
      function s(e) {
        var n;
        if (!window.__appHasPendingCacheUpdate__) {
          try {
            n = JSON.parse(e);
          } catch (e) {
            (n = {}), console.warn("Bridge dispatch failed. err = " + e);
          }
          var i;
          if (n.responseId) {
            if (!(i = r[n.responseId])) return;
            i(n.responseData), delete r[n.responseId];
          } else {
            if (n.callbackId) {
              var t = n.callbackId;
              i = function (e) {
                o({ responseId: t, responseData: e });
              };
            }
            var d = window.WebViewJavascriptBridge._messageHandler;
            if ((n.handlerName && (d = a[n.handlerName]), void 0 === d)) return;
            for (var s in d)
              if (d.hasOwnProperty(s))
                try {
                  d[s](n.data, i);
                } catch (e) {
                  "undefined" != typeof console &&
                    console.log(
                      "WebViewJavascriptBridge: WARNING: javascript handler threw.",
                      n,
                      e
                    );
                }
          }
        }
      }
      function w(e) {
        var n = !1;
        try {
          n = null != a[e];
        } catch (e) {
          n = !1;
        }
        return n ? "true" : "false";
      }
      function c(e) {
        window.gabridge && window.gabridge.onHasHandler(e, w(e));
      }
      window.WebViewJavascriptBridge = {
        init: function (e) {
          if (window.WebViewJavascriptBridge._messageHandler)
            return console.warn("WebViewJavascriptBridge.init called twice"), null;
          window.WebViewJavascriptBridge._messageHandler = [e];
          var n = i;
          (i = null),
            setTimeout(function () {
              if (n) for (var e = 0; e < n.length; e++) s(n[e]);
            }, 0);
        },
        send: function (e, n) {
          o({ data: e }, n);
        },
        registerHandler: function (e, n, i) {
          void 0 === a[e] && (a[e] = {}),
            (i = i || n.toString()),
            (a[e][i] = n),
            c(e);
        },
        unregisterHandler: function (e, n) {
          if (n) {
            var i = a[e];
            if (i) {
              delete i[n];
              var r = !1;
              for (var t in i)
                if (i.hasOwnProperty(t)) {
                  r = !0;
                  break;
                }
              r || (delete a[e], c(e));
            }
          }
        },
        callHandler: function (e, n, i) {
          for (var a in d)
            if (d.hasOwnProperty(a))
              try {
                d[a](e, n);
              } catch (e) {}
          if ("fullScreenImage" === e && n) {
            var r = n.imageUrls;
            if (r && r.length)
              try {
                for (var t = 0; t < r.length; t++) {
                  var s = r[t];
                  "string" != typeof s ||
                    s.indexOf(window.ITEM_IMAGE_BASE_URL) < 0 ||
                    (r[t] = s.replace("/webp/", "/"));
                }
              } catch (e) {}
          }
          o({ handlerName: e, data: n }, i);
        },
        hasHandler: w,
        hasHandlerCB: c,
        addHook: function (e) {
          var n = e.toString();
          d[n] || (d[n] = e);
        },
        delHook: function (e) {
          var n = e.toString();
          delete d[n];
        },
        appHasHandler: function (e) {
          return (
            window.WebViewJavascriptBridge._appHandlers &&
            window.WebViewJavascriptBridge._appHandlers.indexOf(e) >= 0
          );
        },
        _fetchQueue: function () {
          var e = JSON.stringify(n);
          return (n = []), e;
        },
        _handleMessageFromObjC: function (e) {
          i ? i.push(e) : s(e);
        },
        reset: function () {
          (n = []),
            (i = []),
            (a = {}),
            (r = {}),
            (t = 1),
            (window.WebViewJavascriptBridge._messageHandler = null);
        },
      };
      var l = document;
      !(function (n) {
        ((e = n.createElement("iframe")).style.display = "none"),
          (e.src = CUSTOM_PROTOCOL_SCHEME + "://" + QUEUE_HAS_MESSAGE),
          n && n.documentElement && n.documentElement.appendChild(e);
      })(l);
      var f = l.createEvent("Events");
      f.initEvent("WebViewJavascriptBridgeReady"),
        (f.bridge = window.WebViewJavascriptBridge),
        l.dispatchEvent(f);
    })(),
      (window.connectWebViewJavascriptBridge = function (e) {
        window.WebViewJavascriptBridge
          ? e(window.WebViewJavascriptBridge)
          : document.addEventListener(
              "WebViewJavascriptBridgeReady",
              function () {
                (window.dismissNonShopeeBridge &&
                  window.dismissNonShopeeBridge()) ||
                  e(window.WebViewJavascriptBridge);
              },
              !1
            );
      });

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    var OS;
    (function (OS) {
        OS["ANDROID"] = "android";
        OS["IOS"] = "ios";
        OS["OTHER"] = "other";
    })(OS || (OS = {}));
    var userAgentChecker = function () {
        var ua = navigator.userAgent || navigator.vendor;
        var isInApp = false;
        var os = OS.OTHER;
        if (/beeshop/i.test(ua) || /shopee/i.test(ua)) {
            isInApp = true;
        }
        if (isInApp) {
            if (/android/i.test(ua)) {
                os = OS.ANDROID;
            }
            else if (/iPad|iPhone|iPod/i.test(ua)) {
                os = OS.IOS;
            }
        }
        return { isInApp: isInApp, os: os };
    };
    var isOnMobileApp = function () { return userAgentChecker().isInApp; };
    var getAppPath = function (url) {
        var a = { paths: [{ webNav: { url: url } }] };
        var b = btoa(JSON.stringify(a));
        return "home?navRoute=" + encodeURIComponent(b);
    };

    var LoginRespStatus;
    (function (LoginRespStatus) {
        LoginRespStatus[LoginRespStatus["FAILED"] = 0] = "FAILED";
        LoginRespStatus[LoginRespStatus["SUCCESS"] = 1] = "SUCCESS";
        LoginRespStatus[LoginRespStatus["CANCELED"] = 2] = "CANCELED";
    })(LoginRespStatus || (LoginRespStatus = {}));

    var WILL_REAPPEAR_ID = "WILL_REAPPEAR_ID";
    var DID_DISAPPEAR_ID = "DID_DISAPPEAR_ID";
    var WILL_RESIGN_ACTIVE_ID = "WILL_RESIGN_ACTIVE_ID";
    var DID_ACTIVE_ID = "DID_ACTIVE_ID";
    var ShopeeWebBridgeClass = /** @class */ (function () {
        function ShopeeWebBridgeClass() {
            var _this = this;
            /**
             * Initialize web bridge;
             * @returns Returns boolean indicating the successful initialization
             */
            this.init = function () {
                var _a;
                if (isOnMobileApp()) {
                    (_a = _this.WebBridge) === null || _a === void 0 ? void 0 : _a.init();
                    return true;
                }
                return false;
            };
            this.login = function (options, callback) {
                var o = __assign(__assign({}, options), { redirectPath: getAppPath(options.redirectPath) });
                var cb = function (_a) {
                    var status = _a.status;
                    if (status === LoginRespStatus.SUCCESS) {
                        window.location.reload();
                    }
                };
                _this.callHandler("login", o, callback || cb);
            };
            this.configurePage = function (_a) {
                var _b = _a.title, title = _b === void 0 ? "" : _b, showNavbar = _a.showNavbar;
                if (!isOnMobileApp())
                    return;
                var config = {
                    /** Disable pull to reload */
                    disableReload: 1,
                    /** Disable bounce effect in iOS */
                    disableBounce: 1,
                };
                var navbarInvisibleConfig = {
                    isTransparent: showNavbar ? 0 : 1,
                    hideBackButton: showNavbar ? 0 : 1,
                };
                var navbar = __assign({ title: title }, navbarInvisibleConfig);
                _this.callHandler("configurePage", {
                    config: config,
                    navbar: navbar,
                });
            };
            this.navigateTo = function (url) {
                _this.callHandler("navigate", {
                    target: "_blank",
                    url: url,
                });
            };
            this.setScreenAutolock = function (isEnabled) {
                _this.callHandler("deviceScreenAutoLock", {
                    isEnabled: isEnabled,
                });
            };
            this.regApplicationWillResignActive = function (callback) {
                _this.registerHandler("applicationWillResignActive", callback, WILL_RESIGN_ACTIVE_ID);
            };
            this.unregApplicationWillResignActive = function () {
                _this.unregisterHandler("applicationWillResignActive", WILL_RESIGN_ACTIVE_ID);
            };
            this.regApplicationDidBecomeActive = function (callback) {
                _this.registerHandler("applicationDidBecomeActive", callback, DID_ACTIVE_ID);
            };
            this.unregApplicationDidBecomeActive = function () {
                _this.unregisterHandler("applicationDidBecomeActive", DID_ACTIVE_ID);
            };
            this.registerWillReappear = function (callback) {
                _this.registerHandler("viewWillReappear", callback, WILL_REAPPEAR_ID);
            };
            this.registerDidDisappear = function (callback) {
                _this.registerHandler("viewDidDisappear", callback, DID_DISAPPEAR_ID);
            };
            this.unregisterWillReappear = function () {
                _this.unregisterHandler("viewWillReappear", WILL_REAPPEAR_ID);
            };
            this.unregisterDidDisappear = function () {
                _this.unregisterHandler("viewDidDisappear", DID_DISAPPEAR_ID);
            };
        }
        Object.defineProperty(ShopeeWebBridgeClass.prototype, "WebBridge", {
            get: function () {
                return window.WebViewJavascriptBridge;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShopeeWebBridgeClass.prototype, "callHandler", {
            get: function () {
                var _a;
                return (_a = this.WebBridge) === null || _a === void 0 ? void 0 : _a.callHandler;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShopeeWebBridgeClass.prototype, "registerHandler", {
            get: function () {
                var _a;
                return (_a = this.WebBridge) === null || _a === void 0 ? void 0 : _a.registerHandler;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ShopeeWebBridgeClass.prototype, "unregisterHandler", {
            get: function () {
                var _a;
                return (_a = this.WebBridge) === null || _a === void 0 ? void 0 : _a.unregisterHandler;
            },
            enumerable: false,
            configurable: true
        });
        return ShopeeWebBridgeClass;
    }());
    new ShopeeWebBridgeClass();

    var instance = new ShopeeWebBridgeClass();

    exports.instance = instance;

    Object.defineProperty(exports, '__esModule', { value: true });

}));
//# sourceMappingURL=shopeeWebBridge.js.map
