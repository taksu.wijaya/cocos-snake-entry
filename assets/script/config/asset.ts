import { ASSET_TYPE } from "../lib/enum/asset";
import { AssetConfig } from "../lib/interface/asset";
import { ASSET_KEY } from "../enum/AssetScript";

export function getAssets(){
    const assets = new Array<AssetConfig<ASSET_KEY>>();

    //Sprites
    assets.push({
        key:ASSET_KEY.KEYPAD_SPRITE,
        type: ASSET_TYPE.SPRITESHEET,
        url: '',
        localUrl: 'image/keypad',
        config:{
            frameWidth: 137,
            frameHeight: 132,
            paddingX:1,
            paddingY:0
        }
    });
    assets.push({
        key:ASSET_KEY.APPLE_SPRITE,
        type: ASSET_TYPE.IMAGE,
        url:'',
        localUrl: 'image/sprite_apple'
    })
    assets.push({
        key:ASSET_KEY.SOUND_OFF_SPRITE,
        type: ASSET_TYPE.IMAGE,
        url: '',
        localUrl: 'image/sprite_sound_off'
    })
    assets.push({
        key:ASSET_KEY.SOUND_ON_SPRITE,
        type: ASSET_TYPE.IMAGE,
        url: '',
        localUrl: 'image/sprite_sound_on'
    })
    assets.push({
        key: ASSET_KEY.TILE_SPRITE,
        type: ASSET_TYPE.IMAGE,
        url:'',
        localUrl:'image/sprite_tile'
    })
    assets.push({
        key: ASSET_KEY.TROPHY_SPRITE,
        type: ASSET_TYPE.IMAGE,
        url:'',
        localUrl:'image/sprite_trophy'
    })
    assets.push({
        key:ASSET_KEY.WALL_SPRITE,
        type: ASSET_TYPE.IMAGE,
        url: '',
        localUrl: 'image/sprite_wall'
    })
    assets.push({
        key:ASSET_KEY.ROUND_SPRITESHEET,
        type:ASSET_TYPE.SPRITESHEET,
        url:'',
        localUrl:'image/spritesheet_round',
        config:{
            frameWidth: 96,
            frameHeight: 96,
            paddingX:1,
            paddingY:0
        }
    })
    assets.push({
        key: ASSET_KEY.SNAKE_LOGO_SPRITE,
        type: ASSET_TYPE.IMAGE,
        url: '',
        localUrl: 'image/logo_shopee_ular'
    })
    assets.push({
        key: ASSET_KEY.SNAKE_SPRITESHEET,
        type: ASSET_TYPE.SPRITESHEET,
        url: '',
        localUrl: 'image/spritesheet_snake',
        config:{
            frameWidth: 97,
            frameHeight: 96,
            paddingX:0,
            paddingY:0
        }
    })

    //Fonts
    assets.push({
        key:ASSET_KEY.SHOPEE_2021_BOLD_FONT,
        type: ASSET_TYPE.FONT,
        url: '',
        localUrl: 'font/Shopee2021/Shopee2021-Bold',
    });

    //Music
    assets.push({
        key: ASSET_KEY.BG_MUSIC,
        type: ASSET_TYPE.AUDIO,
        url: '',
        localUrl: 'audio/bg-music'
    })

    //SFX
    assets.push({
        key:ASSET_KEY.BUTTON_SFX,
        type: ASSET_TYPE.AUDIO,
        url:'',
        localUrl:'audio/button-sfx'
    })
    assets.push({
        key:ASSET_KEY.CRASH_SFX,
        type: ASSET_TYPE.AUDIO,
        url:'',
        localUrl:'audio/crash'
    })
    assets.push({
        key:ASSET_KEY.EAT_SFX,
        type:ASSET_TYPE.AUDIO,
        url:'',
        localUrl:'audio/eat'
    })
    assets.push({
        key:ASSET_KEY.TURN_SFX,
        type:ASSET_TYPE.AUDIO,
        url:'',
        localUrl:'/audio/turn'
    })
    return assets;
}