import { _decorator, Component, IPhysics2DContact, Collider2D, Contact2DType } from 'cc';
import { ManagerScript } from './ManagerScript';
const { ccclass, property } = _decorator;

@ccclass('FoodScript')
export class FoodScript extends Component {
    @property(ManagerScript)
    public readonly managerInstance?: ManagerScript;
   
    start() {
        let collider = this.getComponent(Collider2D);
        if(collider){
            collider.on(Contact2DType.BEGIN_CONTACT, this.onBeginContact, this);
        }
    }
    onBeginContact(selfCollider: Collider2D, otherCollider: Collider2D, contact: IPhysics2DContact | null){
        console.log('Hello')
        //if(otherCollider.node.name==='Player')  this.scheduleOnce(() => this.managerInstance?.foodEaten());
        //this.managerInstance?.foodEaten();
    }
}

