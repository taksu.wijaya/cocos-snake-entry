import { _decorator, Color, Sprite, Vec3, tween, Quat, Prefab, instantiate, RigidBody2D, Tween} from 'cc';
import { SnakeNode } from '../classes/SnakeNode';
import { DIRECTION } from '../enum/direction';
import { ShopeeSprite } from '../classes/shopeeSprite';
const { ccclass, property } = _decorator;


@ccclass('PlayerBodyScript')
export class PlayerBodyScript extends SnakeNode {
    public _dir!: DIRECTION;
    public headLocation:Vec3|null = null;
    public speed!: number
    @property(SnakeNode)
    private head!:SnakeNode;
    @property(SnakeNode)
    public tail?:SnakeNode | null
    @property(Prefab)
    private newBody!:Prefab;
    @property(ShopeeSprite)
    private spriteControl!: ShopeeSprite;
    private moving: boolean = false;
    @property(RigidBody2D)
    private rigidBody!: RigidBody2D 
    private nextDir!: DIRECTION;
    private spawnNew!: boolean;
    onLoad(){
        this.headLocation = new Vec3(this.node.position.x, this.node.position.y);
    }
    start() {
        this.moving = false;
        this._dir = DIRECTION.NONE;
        this.nextDir = DIRECTION.NONE;
        this.speed = 0;
        this.nodePos = this.node.position;
        this.spawnNew = false;
        this.checkIfTail();
        this.activateRigid();
        this.checkStartDirection();
       
    }
    update(deltaTime: number) {
        if(this.nextDir===DIRECTION.NONE){this.nextDir = this.head._dir;}
        if(!this.moving && this.nextDir!= DIRECTION.NONE){
            this.startMovement();
            this.snakeMovementLogic(0.01)
        }     
        if(this.moving && this._dir === DIRECTION.NONE)Tween.stopAll();
    }

    snakeMovementLogic(deltaTime:number){
        this.speed = this.head.speed;
        const animInterval = this.speed*deltaTime;     
        const playerPosition = this.node.position;
        this.changeDirection()  
        this.nodePos = this.node.position;
        const playerX = Math.floor(playerPosition.x);
        const playerY = Math.floor(playerPosition.y);
        this.headLocation = new Vec3(playerX, playerY);
        if(this.spawnNew){
            this.spawnNewBody(deltaTime);
            this.spawnNew = false;
        }
        switch(this._dir){    
            case DIRECTION.UP: 
                tween(this.node).to(animInterval, {position:new Vec3(this.headLocation.x,this.headLocation.y+30)}).call(()=>this.snakeMovementLogic(deltaTime)).start();
                this.moving = true
                break;
            case DIRECTION.DOWN:
                tween(this.node).to(animInterval, {position:new Vec3(this.headLocation.x,this.headLocation.y-30)}).call(()=>this.snakeMovementLogic(deltaTime)).start();
                this.moving = true
                break;
            case DIRECTION.LEFT:
                tween(this.node).to(animInterval, {position:new Vec3(this.headLocation.x-30,this.headLocation.y)}).call(()=>this.snakeMovementLogic(deltaTime)).start();
                this.moving = true
                break;
            case DIRECTION.RIGHT:
                tween(this.node).to(animInterval, {position:new Vec3(this.headLocation.x+30,this.headLocation.y)}).call(()=>this.snakeMovementLogic(deltaTime)).start();
                this.moving = true
                break;        
        }
        
    }
    changeDirection(){
        let quat = new Quat();
        const animInterval = 0.0;
        switch(this.nextDir){
            case DIRECTION.RIGHT: 
                Quat.fromEuler(quat,0,0,-90)
                if(this._dir === DIRECTION.DOWN)tween(this.node).to(0.0, {rotation:quat}).start();
                else tween(this.node).to(animInterval,{rotation:quat})
                .start();
                this._dir = DIRECTION.RIGHT
                break;
            case DIRECTION.LEFT: 
                Quat.fromEuler(quat,0,0,90)
                if(this._dir === DIRECTION.DOWN)tween(this.node).to(0.0, {rotation:quat}).start();
                else tween(this.node).to(animInterval,{rotation:quat})
                .start();
                this._dir = DIRECTION.LEFT
                break;
            case DIRECTION.UP: 
                this._dir = DIRECTION.UP
                Quat.fromEuler(quat,0,0,0)
                tween(this.node).to(animInterval,{rotation:quat,scale:(new Vec3(1,1,0))})
                .start();
                break;
            case DIRECTION.DOWN: 
                this._dir = DIRECTION.DOWN
                //Quat.fromEuler(quat,0,0,0)
                this.node.setRotationFromEuler(0,0,-180)
                // tween(this.node).to(animInterval,{rotation:quat,scale:(new Vec3(1,-1,0))})
                // .start();
                break;
        }
        this.nextDir = this.head._dir;
        
    }
    checkStartDirection(){
        if(this.head.nodePos === null){console.log('no');return;}
        console.log({node:this.node.position, head:this.head.nodePos},'ah');
        let quat = new Quat();
        if(this.node.position.x<this.head.nodePos?.x && this.node.position.y === this.head.nodePos?.y){
            Quat.fromEuler(quat,0,0,-90)
            tween(this.node).to(0.0, {rotation:quat}).start();
        }
        else if(this.node.position.x>this.head.nodePos?.x && this.node.position.y === this.head.nodePos?.y){
            console.log('lef')
            Quat.fromEuler(quat,0,0,90)
            tween(this.node).to(0.0, {rotation:quat}).start();
        }
        else if(this.node.position.y<this.head.nodePos?.y && this.node.position.x === this.head.nodePos?.x){
            this.node.setRotationFromEuler(0,0,-180)
        }
        else if(this.node.position.y>this.head.nodePos?.y && this.node.position.x === this.head.nodePos?.x){
            Quat.fromEuler(quat,0,0,0)
            tween(this.node).to(0.0, {rotation:quat}).start();
        }
    }
    startMovement(){
        if(this.head.nodePos.x>this.node.position.x && this.head.nodePos.y === this.node.position.y)this.nextDir = DIRECTION.RIGHT;
        else if(this.head.nodePos.x<this.node.position.x && this.head.nodePos.y === this.node.position.y)this.nextDir = DIRECTION.LEFT;
        else if(this.head.nodePos.y>this.node.position.y && this.head.nodePos.x === this.node.position.x)this.nextDir = DIRECTION.UP;
        else if(this.head.nodePos.y<this.node.position.y && this.head.nodePos.x === this.node.position.x)this.nextDir = DIRECTION.DOWN;
        this.moving = true;
    }
    animTrigger(){
        const sprites = this.node.getComponent(Sprite)
        tween(this.node)
        .to(0.5, {scale: new Vec3(1.5, 1.0)}, {
            onUpdate(target,ratio){
            (sprites as Sprite).color = Color.GREEN;
        }})
        .to(0.5, {scale: new Vec3(1,1)},  {
            onUpdate(target,ratio){
            (sprites as Sprite).color = Color.WHITE;
        }})
        .call(()=>{
            if(this.tail !== null){
                if(this._dir !== DIRECTION.NONE )this.tail?.animTrigger();
            }
            else{
                this.spawnNew = true;
            }
        })
        .start();
        
    }
    getInitiate(){
        if(this.headLocation===null)return this.node.position;
        console.log('Trigger')
        switch(this._dir){
            case DIRECTION.RIGHT: 
                return new Vec3(this.headLocation.x-30,this.headLocation.y)
            case DIRECTION.LEFT: 
                return new Vec3(this.headLocation.x+30,this.headLocation.y)
            case DIRECTION.UP: 
                return new Vec3(this.headLocation.x,this.headLocation.y-30)
            case DIRECTION.DOWN: 
                return new Vec3(this.headLocation.x,this.headLocation.y+30)
        }
    }
    spawnNewBody(deltaTime:number){
        if(this.headLocation===null)return this.node.position;
        let newBody = instantiate(this.newBody);
        newBody.setScale(new Vec3(0,0,0));
        newBody.setPosition(this.getInitiate() as Vec3);
        newBody.parent = this.node.getParent();
        tween(newBody)
        .delay(this.speed * deltaTime * 1)
        .to(this.speed * deltaTime *2, {scale: new Vec3(1,1,0)})
        .start();
        let test = newBody.getComponent(PlayerBodyScript);
        test?.linkedHead(this.node.getComponent('SnakeNode') as SnakeNode, this.newBody);
        this.tail = newBody.getComponent('SnakeNode') as SnakeNode;      
        this.checkIfTail();
    }
    activateRigid(){
        this.rigidBody.enabled = true;
    }
    linkedHead(val:SnakeNode, bodyPrefab:Prefab){
        this.head = val;
        this.newBody = bodyPrefab;
    }
    checkIfTail(){
        if(this.tail === null){
            this.spriteControl.frameKey = '2';
        }
        else{
            this.spriteControl.frameKey = '3';
        }
    }

}

