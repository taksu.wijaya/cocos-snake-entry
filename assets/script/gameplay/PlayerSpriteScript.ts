import { _decorator, Component, Node } from 'cc';
import { ShopeeSprite } from '../classes/shopeeSprite';
import { generateAnimationClip } from '../lib/util/animation';
import { ANIMATION_KEY } from '../enum/animation';
import { ASSET_KEY } from '../enum/AssetScript';
const { ccclass, property } = _decorator;

@ccclass('PlayerSpriteScript')
export class PlayerSpriteScript extends ShopeeSprite {
    private readonly IDLE_SNAKE_KEY = ANIMATION_KEY.SNAKE_IDLE_ANIM;

    constructor(){
        super('SnakeSprite', ASSET_KEY.ROUND_SPRITESHEET, 0);
    }
    onLoad(){
        super.onLoad();
    }


}

