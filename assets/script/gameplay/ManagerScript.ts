import { _decorator, Component, Node,instantiate, director, Prefab, Tween, Vec2, Vec3, Game, tween, tiledLayerAssembler, Sprite} from 'cc';
import { ShopeeLabel } from '../classes/shopeeLabel';
import { SCENE_KEY } from '../enum/scene';
import { transition } from '../utils/transition';
import { PlayerBodyScript } from './PlayerBodyScript';
import { SnakeNode } from '../classes/SnakeNode';
import { tilescript } from '../utils/tilescript';
import { PlayerScript } from './PlayerScript';
import { DIRECTION } from '../enum/direction';
const { ccclass, property } = _decorator;
const preDefinedFile = [{
    level: [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    ],
    player:[
        {
            x:0,
            y:1,
            dir: DIRECTION.DOWN
        },
        {
            x:0,
            y:0
        },
        {
            x:1,
            y:0
        }
    ]
},{
level:
    [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0],
        [0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0],
        [0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1],
        [0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1],
        [0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0],
        [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0],
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    ],
player:[
    {
        x:11,
        y:6,
        dir: DIRECTION.UP
    },
    {
        x:11,
        y:7
    },
    {
        x:11,
        y:8
    },
    {
        x:10,
        y:8
    },

]},{
level:
    [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0],
        [0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
],
player:[
    {
        x:4,
        y:7,
        dir: DIRECTION.LEFT
    },
    {
        x:5,
        y:7
    },
    {
        x:5,
        y:6
    },
    {
        x:4,
        y:6
    }
]

}
]
const testTile = [
    [
        // [0, 0, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4],
        // [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4],
        // [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4],
        // [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4],
        // [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4],
        // [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4],
        // [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4],
        // [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4],
        // [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4],
        // [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4],
        // [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4],
        // [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4],
        // [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4],
        [1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 4, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 4, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 4, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 4, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 4, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 4, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 4, 1, 1, 1, 1, 1, 1],
    ]
]
@ccclass('ManagerScript')
export class ManagerScript extends Component {
    @property({type: Prefab})
    private TileNode?:  Node;
    @property({type: Prefab})
    private WallNode?: Node;
    @property(Node)
    private AppleFood?: Node;
    @property(ShopeeLabel)
    private LabelPoint?: ShopeeLabel
    @property(Node)
    private SpawnLocation!:Node;
    @property(Node)
    private PlayerSpawn!:Node;
    private initialPoint: number = 0
    @property(Node)
    private GameOverModal !:Node
    @property(ShopeeLabel)
    private LabelHighSCore?: ShopeeLabel
    @property(ShopeeLabel)
    private GameOver!: ShopeeLabel
    @property(Node)
    private Invalid!: Node
    private tiles!: Array<number>[]

    @property(Node)
    private Player !:Node
    @property(Node)
    private PlayerBody !:Node
    @property(Prefab)
    private PlayerSubBody !:Node
    @property(transition)
    private BlackScreen!:transition;
    private listBody !: Array<Node>;
    private tileState!: Array<number>[];
    private invalidFlag!: boolean;
    private playerLocation!: Array<{x:number, y:number, dir?:DIRECTION}>;
    onLoad(){
        this.listBody = [];
        const levelChosen = JSON.parse(JSON.stringify(preDefinedFile[Math.floor(Math.random()*preDefinedFile.length)]));
        this.tiles = levelChosen.level;
        this.tileState = Array.from(this.tiles);
        this.playerLocation = levelChosen.player;
        this.invalidFlag = false;
    }
    start() {
        this.BlackScreen.hdnFadeIn()
        let highScore = localStorage.getItem('highscore');
        if(highScore===null)highScore='0';
        this.LabelHighSCore?.setText(highScore);
        this.generateTiles();
        this.initialPoint = 0;
        this.GameOverModal.setPosition(1000,118)
        this.Invalid.setPosition(1000,118)
        if(this.invalidFlag)(this.hdnInvalid());
    }
    mutateTile(x:number,y:number,state:number){
        if(this.tileState[y][x]===2 && state===3){
            console.log(this.tileState[y][x]===2,'mutate')
            this.tileState[y][x] = state;
            this.scheduleOnce(()=>this.foodEaten());
        }
        else{
            this.tileState[y][x] = state;
        }
        
      
    }
    generateTiles(){
        this.AppleFood?.setParent(this.PlayerSpawn);
        this.tiles.forEach((tileRow, rowidx)=>{
            tileRow.forEach((tile, idx)=>{
                let TileInst = instantiate(this.TileNode) as Node; 
                TileInst.getComponent(tilescript)?.setUp(idx,rowidx,this.node.getComponent(ManagerScript) as ManagerScript)
                let WallInst = instantiate(this.WallNode) as Node;
                if(TileInst && tile === 0) {
                    TileInst.parent = this.SpawnLocation;
                    TileInst?.setPosition(idx*30,-rowidx*30,0);
                }
                else if(tile === 1){
                    WallInst.parent = this.SpawnLocation;
                    WallInst?.setPosition(idx*30,-rowidx*30,0);
                }
                else if(tile === 2){
                    this.spawnFood(idx,rowidx);
                    TileInst.parent = this.SpawnLocation;
                    TileInst?.setPosition(idx*30,-rowidx*30,0);
                }
                else if(tile===3 ){
                    TileInst.parent = this.SpawnLocation;
                    TileInst?.setPosition(idx*30,-rowidx*30,0);
                    this.Player.setPosition(idx*30,-rowidx*30,0);
                }
                else if(tile===4){
                    TileInst.parent = this.SpawnLocation;
                    TileInst?.setPosition(idx*30,-rowidx*30,0);
                    if(this.listBody.length>0){
                        let newBody = instantiate(this.PlayerSubBody);
                        const head = this.listBody[this.listBody.length-1].getComponent(PlayerBodyScript)
                        newBody.parent = this.PlayerSpawn;
                        newBody.setPosition(idx*30,-rowidx*30,0)
                        let test = newBody.getComponent(PlayerBodyScript);
                        test?.linkedHead(this.listBody[this.listBody.length-1].getComponent('SnakeNode') as SnakeNode, this.PlayerSubBody as unknown as Prefab);
                        (head as PlayerBodyScript).tail = newBody.getComponent('SnakeNode') as SnakeNode;      
                        (head as PlayerBodyScript).checkIfTail();
                        this.listBody.push(newBody);
                        
                    }
                    else{
                        this.PlayerBody.setPosition(idx*30,-rowidx*30,0);
                        this.PlayerBody.getComponent(PlayerBodyScript)?.checkStartDirection();
                        this.listBody.push(this.PlayerBody);
                    }
                   
                }
            })
        })
        if(this.playerLocation){
            this.spawnPlayer();
        }
       this.spawnFood();
    }
    spawnPlayer(){
        for(let i =0; i<this.playerLocation.length;i++){
            const coorX = this.playerLocation[i].x;
            const coorY = this.playerLocation[i].y
            if(this.tileState[coorY][coorX]!==0 || this.playerLocation.length<3 ){
                this.invalidFlag = true; 
                break;
            }
            if(i===0){
                this.Player.setPosition(coorX*30, -coorY*30);
                this.tileState[coorY][coorX] = 2;
                switch(this.playerLocation[i].dir){
                    case DIRECTION.UP:
                        this.Player.setRotationFromEuler(0,0,0)
                        break;
                    case DIRECTION.RIGHT:
                        this.Player.setRotationFromEuler(0,0,-90)
                        break;
                    case DIRECTION.LEFT:
                        this.Player.setRotationFromEuler(0,0,90)
                        break;
                    case DIRECTION.DOWN:
                        this.Player.setRotationFromEuler(0,0,-180)
                        break;
               }
               this.tileState[coorY][coorX] = 3;
            }
            else{
                console.log(Math.abs(coorX-this.playerLocation[i-1].x), Math.abs(coorY-this.playerLocation[i-1].y),'aaaa')
                if(Math.abs(coorX-this.playerLocation[i-1].x) > 1 || Math.abs(coorY-this.playerLocation[i-1].y)>1){
                    this.invalidFlag = true;
                    break;
                }
                if(this.listBody.length>0){
                    let newBody = instantiate(this.PlayerSubBody);
                    const head = this.listBody[this.listBody.length-1].getComponent(PlayerBodyScript)
                    newBody.parent = this.PlayerSpawn;
                    newBody.setPosition(coorX*30,-coorY*30,0)
                    let test = newBody.getComponent(PlayerBodyScript);
                    test?.linkedHead(this.listBody[this.listBody.length-1].getComponent('SnakeNode') as SnakeNode, this.PlayerSubBody as unknown as Prefab);
                    (head as PlayerBodyScript).tail = newBody.getComponent('SnakeNode') as SnakeNode;      
                    (head as PlayerBodyScript).checkIfTail();
                    this.listBody.push(newBody);
                    this.tileState[coorY][coorX] = 4;
                    
                }
                else{
                    this.PlayerBody.setPosition(coorX*30,-coorY*30,0);
                    this.listBody.push(this.PlayerBody);
                    this.tileState[coorY][coorX] = 4;
                }
            }
        }
    }
    hdnPlayAgain(){
        director.loadScene(SCENE_KEY.PLAY);
    }
    hdnToTitle(){
        this.BlackScreen.hdnFadeOut();
        setTimeout(()=>director.loadScene(SCENE_KEY.TITLE),1000);
       
    }
    foodEaten(){
        this.initialPoint++
        this.spawnFood();
        this.LabelPoint?.setText((this.initialPoint).toString());
        this.Player.getComponent(PlayerScript)?.expandBody();
    }

    spawnFood(x?:number, y?:number){
        const coor = this.getRandomCoordinate()
        const newPos = new Vec3(x ? x*30 :  coor[0]*30, y ? -y*30: -coor[1]*30)
        //tween(this.AppleFood).to(0.0,{position:newPos});
        //this.AppleFood.setPosition(newPos);
        console.log(this.tileState,'Eaten',{x:coor[0],y:coor[1], res: this.tileState[coor[1]][coor[0]]});
        this.tileState[y ?? coor[1]][x ?? coor[0]]=2;
        this.AppleFood?.setPosition(newPos);
 
        //console.log(this.tileState, {x, y})
       
        
    }
    hdnGameOver(){
        Tween.stopAll()
        this.GameOverModal.setPosition(-5,118)
        const highScore = localStorage.getItem('highscore');
        if(highScore===null)localStorage.setItem('highscore',this.initialPoint.toString());
        else if(parseInt(highScore)<this.initialPoint){
            localStorage.setItem('highscore',this.initialPoint.toString());
        }
        this.GameOver.setText(this.initialPoint.toString())
    }
    hdnInvalid(){
        Tween.stopAll()
        this.Invalid.setPosition(-5,118)
    }
    getRandomCoordinate() {
        let coordinateX = (Math.floor(Math.random() * 11));
        let coordinateY = (Math.floor(Math.random() * 12));
        
        while(this.tileState[coordinateY][coordinateX] !== 0){
             coordinateX = (Math.floor(Math.random() * 11));
             coordinateY = (Math.floor(Math.random() * 12));
        }
        return [coordinateX,coordinateY];
    }
}

