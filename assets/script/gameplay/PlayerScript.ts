import { _decorator, Component, Node, RigidBody2D, 
    Contact2DType,Collider2D, input, Input, EventKeyboard, KeyCode, Vec3, Quat, 
    UITransform, director, tween, IPhysics2DContact, Tween
 } from 'cc';
import { SnakeNode } from '../classes/SnakeNode';
import { ManagerScript } from './ManagerScript';
import { DIRECTION } from '../enum/direction';
import { ShopeeAudio } from '../classes/shopeeAudio';
import { ASSET_KEY } from '../enum/AssetScript';
import { AudioManager } from '../miscellaneous/audio/AudioManager';
const { ccclass, property } = _decorator;


@ccclass('PlayerScript')
export class PlayerScript extends SnakeNode {
    public _dir!: DIRECTION;
    private winState:boolean = true;
    private isDirChange: boolean = false;
    private pendingDir: DIRECTION = DIRECTION.NONE;
    private eating!: boolean;
    private moving!: boolean;
    public headLocation: Vec3| null = null;
    @property
    public speed!:number;
    @property(Number)
    public interValUpdate!:number
    @property(Number)
    public speedIncrease!:number
    @property(ManagerScript)
    public readonly managerInstance?: ManagerScript;
    @property(SnakeNode)
    public tail!:SnakeNode
    @property(ShopeeAudio)
    private eatSFX!:ShopeeAudio;
    @property(ShopeeAudio)
    private turnSFX!:ShopeeAudio;
    @property(ShopeeAudio)
    private wallSFX!:ShopeeAudio;
    private counter!:number
    onLoad(){
       input.on(Input.EventType.KEY_DOWN, this.movement, this) ;
       this.headLocation = new Vec3(this.node.position.x, this.node.position.y);
    }
    start(){
        this.eating = false;
        this.nodePos = this.node.position;
        this._dir = DIRECTION.NONE;
        this.winState = true;
        let collider = this.getComponent(Collider2D);
        this.moving = false;
        this.counter = 0;
        if (collider) {
            collider.on(Contact2DType.BEGIN_CONTACT, this.onBeginContact, this);
           // collider.on(Contact2DType.END_CONTACT, this.onEndContact, this);
          }
    }
    buttonInput(event: Event, customEventData: string){
        if(this._dir === DIRECTION.NONE){
            this.directionCheck()
        }
        switch(customEventData){
            case 'RIGHT': 
                if(this._dir === DIRECTION.LEFT) break;
                this.pendingDir = DIRECTION.RIGHT;
                this.isDirChange =  true;
                break;
            case 'LEFT':
                if(this._dir === DIRECTION.RIGHT) break;
                this.pendingDir = DIRECTION.LEFT;
                this.isDirChange =  true;
                break;
            case 'UP':
                if(this._dir === DIRECTION.DOWN) break;
                this.pendingDir = DIRECTION.UP;
                this.isDirChange =  true;
                break;
            case 'DOWN':
                if(this._dir === DIRECTION.UP) break;
                this.pendingDir = DIRECTION.DOWN;
                this.isDirChange =  true;
                break;
        }
        if(!this.moving){this.snakeMovementLogic(0.01);}
    }
    movement(event:EventKeyboard){ 
        if(this._dir === DIRECTION.NONE){
            this.directionCheck()
        }
        switch(event.keyCode){
            case KeyCode.ARROW_RIGHT: 
                if(this._dir === DIRECTION.LEFT) break;
                this.pendingDir = DIRECTION.RIGHT;
                this.isDirChange =  true;
                break;
            case KeyCode.ARROW_LEFT:
                if(this._dir === DIRECTION.RIGHT) break;
                this.pendingDir = DIRECTION.LEFT;
                this.isDirChange =  true;
                break;
            case KeyCode.ARROW_UP:
                if(this._dir === DIRECTION.DOWN) break;
                this.pendingDir = DIRECTION.UP;
                this.isDirChange =  true;
                break;
            case KeyCode.ARROW_DOWN:
                if(this._dir === DIRECTION.UP) break;
                this.pendingDir = DIRECTION.DOWN;
                this.isDirChange =  true;
                break;
        }
        if(!this.moving)this.snakeMovementLogic(0.01);
    }
    directionCheck(){
        console.log(this.node.rotation.z,'directioncheck');
       switch(this.node.getRotation().z){
            case 0:
                this._dir = DIRECTION.UP;
                break;
            case -0.7071067811865475:
                this._dir = DIRECTION.RIGHT;
                break;
            case 0.7071067811865475:
                this._dir = DIRECTION.LEFT;
                break;
            case -1:
                this._dir = DIRECTION.DOWN;
            break;
       }
    }
    onBeginContact(selfCollider: Collider2D, otherCollider: Collider2D, contact: IPhysics2DContact | null) {
        if(otherCollider.node.name==='SnakeBody' || otherCollider.node.name==='WallNode'){
            this._dir =DIRECTION.NONE;
            if(localStorage.getItem('audio')==='1')this.wallSFX.play();
            Tween.stopAll()
            this.managerInstance?.hdnGameOver();
        }
       
    }
    // onEndContact(selfCollider: Collider2D, otherCollider: Collider2D, contact: IPhysics2DContact | null) {
    //     if(otherCollider.node.name==='AppleFood'){
    //         this.expandBody();

    //     }
    // }
    expandBody(){
        if(localStorage.getItem('audio')==='1')this.eatSFX.play();
        this.tail.animTrigger();
        this.counter++;
        if(this.counter%this.interValUpdate === 0)this.speed-=this.speedIncrease;
    }
    changeDirection(){
        let quat = new Quat();
        const animInterval = this.speed * 0.01;
        switch(this.pendingDir){
            case DIRECTION.RIGHT: 
                Quat.fromEuler(quat,0,0,-90)
                if(localStorage.getItem('audio')==='1')this.turnSFX.play();
                if(this._dir === DIRECTION.DOWN || this._dir === DIRECTION.NONE)tween(this.node).to(0.0, {rotation:quat}).start();
                else tween(this.node).to(animInterval,{rotation:quat, scale:(new Vec3(1,1,0))}).start();
                this._dir = DIRECTION.RIGHT
                break;
            case DIRECTION.LEFT: 
                Quat.fromEuler(quat,0,0,90)
                if(localStorage.getItem('audio')==='1')this.turnSFX.play();
                if(this._dir === DIRECTION.DOWN || this._dir === DIRECTION.NONE)tween(this.node).to(0.0, {rotation:quat}).start();
                else tween(this.node).to(animInterval,{rotation:quat, scale:(new Vec3(1,1,0))})
                .start();
                this._dir = DIRECTION.LEFT
                break;
            case DIRECTION.UP: 
                if(localStorage.getItem('audio')==='1')this.turnSFX.play();
                this._dir = DIRECTION.UP
                Quat.fromEuler(quat,0,0,0)
                tween(this.node).to(animInterval,{rotation:quat, scale:(new Vec3(1,1,0))})
                .start();
                break;
            case DIRECTION.DOWN: 
                if(localStorage.getItem('audio')==='1')this.turnSFX.play();
                this._dir = DIRECTION.DOWN
                this.node.setRotationFromEuler(0,0,-180)
                //Quat.fromEuler(quat,0,0,-180)
                // tween(this.node).to(animInterval,{rotation:quat})
                // .start();
                break;
        }
        
        this.pendingDir = DIRECTION.NONE;
        
    }
    snakeMovementLogic(deltaTime:number){
        const playerPosition = this.node.position;
        const playerX = Math.floor(playerPosition.x);
        const playerY = Math.floor(playerPosition.y);
        if(this.pendingDir !== this._dir)this.changeDirection()
        this.nodePos = this.node.position;
        this.headLocation = new Vec3(playerX, playerY);
        switch(this._dir){
            case DIRECTION.UP: 
                tween(this.node).to(this.speed*deltaTime, {position:new Vec3(this.node.position.x,this.node.position.y+30)}).call(()=>this.snakeMovementLogic(deltaTime)).start();
                this.moving = true
                break;
            case DIRECTION.DOWN:
                tween(this.node).to(this.speed*deltaTime, {position:new Vec3(this.node.position.x,this.node.position.y-30)}).call(()=>this.snakeMovementLogic(deltaTime)).start();
                this.moving = true
                break;
            case DIRECTION.LEFT:
                tween(this.node).to(this.speed*deltaTime, {position:new Vec3(this.node.position.x-30,this.node.position.y)}).call(()=>this.snakeMovementLogic(deltaTime)).start();
                this.moving = true
                break;
            case DIRECTION.RIGHT:
                tween(this.node).to(this.speed*deltaTime, {position:new Vec3(this.node.position.x+30,this.node.position.y)}).call(()=>this.snakeMovementLogic(deltaTime)).start();
                this.moving = true
                break;        
        }

        
    }
    
}

