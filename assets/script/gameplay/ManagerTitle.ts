import { _decorator, Component, Node, director } from 'cc';
import { ShopeeLabel } from '../classes/shopeeLabel';
import { SCENE_KEY } from '../enum/scene';
import { transition } from '../utils/transition';
const { ccclass, property } = _decorator;

@ccclass('ManagerTitle')
export class ManagerTitle extends Component {
    @property(ShopeeLabel)
    private highScore!: ShopeeLabel
    @property(transition)
    private blackScreen!:transition
    start() {
        this.blackScreen.hdnFadeIn();
        let highScore = localStorage.getItem('highscore');
        if(highScore===null)highScore='0'
        this.highScore.setText(highScore);
    }
    hdnPlay(){
        this.blackScreen.hdnFadeOut();
        setTimeout(()=> director.loadScene(SCENE_KEY.PLAY),1000);  
    }
}

