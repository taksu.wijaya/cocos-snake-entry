import { _decorator, Component, Node } from 'cc';
import { BaseAudio } from '../lib/classes/baseAudio';
import { ASSET_KEY } from '../enum/AssetScript';
const { ccclass, property } = _decorator;

const AudioProperty = {
    type:ASSET_KEY,
    visible: true,
    override: true
}

@ccclass('ShopeeAudio')
export class ShopeeAudio extends BaseAudio<ASSET_KEY> {
    @property(AudioProperty)
    public assetKey = ASSET_KEY.NONE;
}

