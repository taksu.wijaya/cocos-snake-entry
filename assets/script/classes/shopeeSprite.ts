import { BaseSprite } from '../lib/classes/baseSprite';
import { _decorator, Component, Node } from 'cc';
import { ASSET_KEY } from '../enum/AssetScript';
const { ccclass, property } = _decorator;

const ShopeeSpriteProps = {
    type:ASSET_KEY,
    visible:true,
    override:true
}

@ccclass('ShopeeSprite')
export class ShopeeSprite extends BaseSprite<ASSET_KEY> {
    @property(ShopeeSpriteProps)
    assetKey = ASSET_KEY.NONE;
}

