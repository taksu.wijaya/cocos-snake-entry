import { BaseLabel } from "../lib/classes/baseLabel";
import { _decorator } from "cc";
import { ASSET_KEY } from "../enum/AssetScript";
const {ccclass, property}= _decorator;

const LabelProps = {
    type:ASSET_KEY,
    visible: true,
    override:true
}

@ccclass('ShopeeLabel')
export class ShopeeLabel extends BaseLabel<ASSET_KEY>{
    @property(LabelProps)
    assetKey = ASSET_KEY.NONE;
}
