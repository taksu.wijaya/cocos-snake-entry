import { _decorator, Component, Node, Vec3 } from 'cc';
import { DIRECTION } from '../enum/direction';
const { ccclass, property } = _decorator;


@ccclass('SnakeNode')
export abstract class SnakeNode extends Component {
    public _dir!: DIRECTION;
    public headLocation!: Vec3|null;
    public speed!:number;
    public nodePos!: Vec3;
    animTrigger(){};
}

