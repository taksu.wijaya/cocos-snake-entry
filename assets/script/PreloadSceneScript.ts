import { _decorator, Component, Node, director, Scene, game, AudioSource } from 'cc';
import { ASSET_KEY } from './enum/AssetScript';
import {PreloadSceneComponent} from './interface/PreloadSceneComponentScript';
import { BaseLoader } from './lib/classes/baseLoader';
import { SCENE_KEY } from './enum/scene';
import { AssetLoadingUI } from './object/assetLoadingUI';
import { ShopeeAudio } from './classes/shopeeAudio';
import { ShopeeSprite } from './classes/shopeeSprite';
import ShopeeWebBridge from 'db://shopee-web-bridge/shopeeWebBridge.js';
import { loadErrorPageBundle, showErrorPage } from './lib/util/errorPageWraper';
import { integrateGameToWindow } from './utils/integration';
import { ASSET_LOADER_EVENT } from './lib/enum/assetLoader';
import { RETRY_STATUS } from './lib/enum/retry';
import { getAssets } from './config/asset';
import { transition } from './utils/transition';
import { AudioManager } from './miscellaneous/audio/AudioManager';
const { ccclass, property } = _decorator;

@ccclass('PreloadSceneScript')
export class PreloadSceneScript extends Component implements PreloadSceneComponent {
    //Interface Requirement
    @property(BaseLoader)
    public readonly assetLoader?: BaseLoader<ASSET_KEY>;
    @property(Node)
    public readonly preloadControl?: Node;
    @property(transition)
    public readonly BlackScreen!:transition;

    public goToTitleScene(){
        this.BlackScreen.hdnFadeOut();
        setTimeout(()=>director.loadScene(SCENE_KEY.TITLE),1000);
        
    }
    //

    @property(AssetLoadingUI)
    public readonly assetLoadingUI?: AssetLoadingUI;
    @property(ShopeeAudio)
    public readonly backgroundMusic?: ShopeeAudio;
    private baseSprite = new Array<ShopeeSprite>();

    onLoad(){
        this.setupWebBridge();
        this.baseSprite = this.node.scene.getComponentsInChildren(ShopeeSprite);
    }
    private setupWebBridge(){
        const isWebBridgeReady = ShopeeWebBridge.instance.init();
        if(!isWebBridgeReady){
            console.log('dont exist');
            return
        }
        ShopeeWebBridge.instance.configurePage({
            showNavbar:true,
            title:'Cocos Snake'
        })
    }
    
    async start(){
        // try{
        //     await loadErrorPageBundle();
        // }
        // catch(err){
        //     throw err;
        // }
        integrateGameToWindow(director);
        this.startAssetLoad();
      
    }

    private startAssetLoad(){
        const {assetLoader} = this;
        assetLoader?.node.on(ASSET_LOADER_EVENT.START, this.onAssetLoaderStart, this);
        assetLoader?.node.on(ASSET_LOADER_EVENT.ASSET_LOAD_SUCCESS, this.onAssetLoadSuccess, this);
        assetLoader?.node.on(ASSET_LOADER_EVENT.ASSET_LOAD_FAILURE, this.onAssetFailure, this);
        assetLoader?.node.on(ASSET_LOADER_EVENT.COMPLETE, this.onAssetLoaderComplete, this);

        assetLoader?.startAssetsLoad(getAssets());
    }
    private onAssetLoaderStart(progress:number){
        this.assetLoadingUI?.updateText(progress);
    }

    private onAssetLoadSuccess(progress:number, key:string){
        this.assetLoadingUI?.updateText(progress,key);
        this.baseSprite?.forEach((sprite)=>{
            sprite.reload();
        });
    }

    private async onAssetFailure(
        progress:number,
        key:string,
        url:string,
        status:RETRY_STATUS,
        e:Error|null)
    {
        if(status === RETRY_STATUS.FAILED){
            await showErrorPage();
        }
    }

    private onAssetLoaderComplete(progress:number){
        this.assetLoadingUI?.updateText(progress);
        this.onComplete();
    }
    private onComplete(){
        this.handleBackgroundMusic();
        this.preloadControl?.once(
            Node.EventType.TOUCH_END,
            this.goToTitleScene,
            this,
        )
    }
    private handleBackgroundMusic(){
        const {backgroundMusic} = this;
        if(!backgroundMusic) return;
        //backgroundMusic.play();

        game.addPersistRootNode(backgroundMusic.node);
        AudioManager.init(this.backgroundMusic as unknown as AudioSource);
        if(localStorage.getItem('audio')==='1' || localStorage.getItem('audio')===null){
            AudioManager.playMusic();
            localStorage.setItem('audio', '1');
        }
    }

}

