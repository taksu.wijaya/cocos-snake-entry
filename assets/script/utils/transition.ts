import { _decorator, Component, Node, Sprite, Color, tween, Tween } from 'cc';
const { ccclass, property } = _decorator;
class BindTarget{
    color: Color = new Color;
}
@ccclass('transition')
export class transition extends Component {
    private fadeFlag = 0
    private colorR=0;
    hdnFadeIn(){
        this.fadeFlag = 2;
        this.colorR = 255;

    }
    hdnFadeOut(){
        this.fadeFlag = 1;
        this.colorR = 0;
    }
    update(deltaTime:number){
        if(this.fadeFlag === 1){
            let sp=  this.node.getComponent(Sprite);
            (sp as Sprite).color = new Color(0,0,0,this.colorR);
            this.colorR+=5;
           if(this.colorR>=255){
                this.fadeFlag = 0;
                this.colorR = 255;
            }
        }
        else if(this.fadeFlag === 2){
            let sp=  this.node.getComponent(Sprite);
            (sp as Sprite).color= new Color(0,0,0,this.colorR);
            this.colorR-=5;
           if(this.colorR<=0){
                this.fadeFlag = 0;
                this.colorR = 0;
            }
        }
        
    }
}

