import { _decorator, Component, Node, Contact2DType, Collider2D } from 'cc';
import { ManagerScript } from '../gameplay/ManagerScript';
import { PlayerBodyScript } from '../gameplay/PlayerBodyScript';
const { ccclass, property } = _decorator;

@ccclass('tilescript')
export class tilescript extends Component {
    private _coordinate!:Array<number>;
    private _manager!: ManagerScript;
    setUp(coorX:number,coorY:number, manager: ManagerScript){
        this._coordinate = [coorX, coorY];
        this._manager = manager;
    }
    start(){
        let collider = this.getComponent(Collider2D);
        if (collider) {
            collider.on(Contact2DType.BEGIN_CONTACT, this.onBeginContact, this);
            collider.on(Contact2DType.END_CONTACT, this.onEndContact, this);
          }
    }
    onBeginContact(selfCollider: Collider2D, otherCollider: Collider2D){
        if(otherCollider.node.name === 'Player')this._manager.mutateTile(this._coordinate[0], this._coordinate[1], 3)
        else if (otherCollider.node.name === 'SnakeBodyMain' || otherCollider.node.name === 'SnakeBody') this._manager.mutateTile(this._coordinate[0], this._coordinate[1], 4)
       
    }
    onEndContact(selfCollider: Collider2D, otherCollider: Collider2D){
        if(otherCollider.node.name === 'SnakeBody' && otherCollider.node.getComponent(PlayerBodyScript)?.tail===null)this._manager.mutateTile(this._coordinate[0], this._coordinate[1], 0)
        else if(otherCollider.node.name === 'Player')this._manager.mutateTile(this._coordinate[0], this._coordinate[1], 4)
    }
}

