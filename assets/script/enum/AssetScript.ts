import { convertEnum, setAssetEnum } from "../lib/util/asset";
import { Enum } from "cc";

export enum ASSET_KEY {
    NONE,
    //Spirte
    KEYPAD_SPRITE,
    SNAKE_LOGO_SPRITE,
    APPLE_SPRITE,
    SOUND_OFF_SPRITE,
    SOUND_ON_SPRITE,
    TILE_SPRITE,
    TROPHY_SPRITE,
    WALL_SPRITE,
    ROUND_SPRITESHEET,
    SNAKE_SPRITESHEET,

    
    //Font
    SHOPEE_2021_BOLD_FONT,
    //Audio
    BG_MUSIC,
    //SFX
    SILENCE_SFX,
    BUTTON_SFX,
    CRASH_SFX,
    EAT_SFX, 
    TURN_SFX
}

convertEnum(ASSET_KEY);
setAssetEnum(ASSET_KEY);
Enum(ASSET_KEY);