import { _decorator, Component, Node } from 'cc';
import { AudioManager } from './AudioManager';
const { ccclass, property } = _decorator;

@ccclass('bgmanager')
export class bgmanager extends Component {
   @property(Node)
   private soundON!:Node;
   @property(Node)
   private soundOFF!:Node;

   start(){
    if(localStorage.getItem('audio')==='1'){
        this.soundON.active = true;
        this.soundOFF.active = false;
    }
    else{
        this.soundON.active = false;
        this.soundOFF.active = true;
    }
   }

   hdnSoundOn(){
    this.soundON.active = true;
    this.soundOFF.active = false;
    localStorage.setItem('audio', `1`);
    AudioManager.playMusic()
   }
   hdnSoundOff(){
    this.soundON.active = false;
    this.soundOFF.active = true;
    localStorage.setItem('audio', `0`);
    AudioManager.pauseMusic()
   }
}

