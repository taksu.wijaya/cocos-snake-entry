import { ShopeeLabel } from '../../classes/shopeeLabel';
import { _decorator} from 'cc';
import { ASSET_KEY } from '../../enum/AssetScript';
const { ccclass, property } = _decorator;

@ccclass('ShopeeLableGet')
export class ShopeeLableGet extends ShopeeLabel {
   constructor(){
    super('Shopee2021BoldLabel', ASSET_KEY.SHOPEE_2021_BOLD_FONT);
   }
}

